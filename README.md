# Hello **_UNIXers_**, here are my dotfiles ;)

# Installation and dependencies

-   **checkupdates** and **cower** : for polybar's update module **only on arch based distros**
-   **compton** : for blur and transparency
-   **even-better-ls** : ls with fancy icons [https://github.com/illinoisjackson/even-better-ls](https://github.com/illinoisjackson/even-better-ls)
-   **lm-sensors** : for polybar's temperatures modules
-   **luakit** : as a lighteweight browser [https://luakit.github.io/](https://luakit.github.io/)
-   **mpd** and **mpc** : for polybar's music module
-   **nerd-font-complete** : collection of monospaced fonts with a **lot** of glyphs [https://github.com/ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
-   **oh-my-zsh** : for the terminal (I use _ppowerlevel9k_ as theme)
-   **python-praw** : for reddit polybar module (you must to get an API key : [see the python-praw doc](http://praw.readthedocs.io/en/latest/getting_started/authentication.html))
-   **taskwarrior** : for task module in polybar

# Compton

Blur and transparency for _i3_

# Dunst
## Scrot

![scrots/dunst.png](scrots/dunst.png)

## Dependencies
-   **numix-icons** : for the icons obviously

# Emacs
## Scrot

![scrots/emacs.png](scrots/emacs.png)

## Plugins

Not the full list...

-   auto-complete
-   company
-   flycheck
-   irony
-   ispell
-   markdown-preview-mode
-   org-mode
-   rainbow-delimiter
-   rainbow-mode
-   smart-line-mode
-   2 personnal themes, one based on _gruvbox_, the other on _molokai_

## Sources

Based on the configuration from _lturpinat_ : <https://github.com/lturpinat/.emacs.d>

# Polybar

## Scrot

![scrots/polybar.png](scrots/polybar.png)

## Modules

### Left group (_mpd_)

From **left** to **right**

-   _mpd_ : current song - artist
-   _mpd_ : current song's style
-   _mpd_ : file extension (cause I love flac)
-   _mpd_ : percentage elapsed
-   _mpd_ : remaining time
-   _mpd_ : current song's position in playlist

### Right group (monitoring)

From **right** tp **left**

-   hour (on click : date)
-   cpu usage per core as graphs
-   cpu total usage as percentage
-   temperature of the GPU
-   temperature of the CPU
-   total ram usage as ram
-   total ram usage as percentage
-   download (ethernet)
-   upload (ethernet)
-   ping (from _www.ping.eu_)
-   updates (archlinux official / AUR)
-   is my server online (just a ping)
-   disk usage (/)
-   available entropy
-   number of unread messages on reddit
-   number of task in taskwarrior
-   keyboard layout
-   number of comments on my website (mysql remote request)

The majority of the modules are just scripts output (see the .sh files)

## Sources

Not precise sources... Few ideas and modules from reddit/unixporn or personnals development

# Rofi

## Scrot

![scrots/rofi.png](scrots/rofi.png)

## Sources

Modified theme based on the work of "_lans9831_" from reddit : <https://www.reddit.com/r/unixporn/comments/7a6c3c/i3gaps_im_the_one_who_knocks/>

# Zsh

## Scrot

![scrots/zsh.png](scrots/zsh.png)

## Plugins

### Zsh plugins (setopts)

-   **autocd** : If a command is issued that can't be executed as a normal command, and the command is the name of a directory, perform the cd command to that directory.
-   **chaselinks** : Resolve symbolic links to their true values when changing directory.
-   **pushdtohome** : Have pushd with no arguments act like 'pushd $HOME'.
-   **pushdsilent** : Do not print the directory stack after pushd or popd.
-   **listrowfirst** : Lay out the matches in completion lists sorted horizontally, that is, the second match is to the right of the first one, not under it as usual.
-   **clobber** : Allows '>' redirection to truncate existing files, and '>>' to create files.
-   **correct** : Try to correct the spelling of commands.

### Oh-my-zsh plugins

-   **archlinux** : Aliases for pacman _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#archlinux)_
-   **copydir** : Copies the curret dir's pathname in clipboard _[(link)](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/copydir/copydir.plugin.zsh)_
-   **copyfile** : Copies the contet of the file in the clipboard _[(link)](https://github.com/robbyrussell/oh-my-zsh/blob/master/plugins/copyfile/copyfile.plugin.zsh)_
-   **cp** : Provide a better `cp` command _[(link)](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/cp)_
-   **docker** : Completion for _docker_ _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#docker)_
-   **extract** : Just use `x` command to extract archive without care of his type _[(link)](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/extract)_
-   **fasd** : Command-line productivity booster _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#fasd)_
-   **fast-syntax-highlighting** : Syntaxic coloration in terminal _[(link)](https://github.com/zdharma/fast-syntax-highlighting)_
-   **history** : Provides a couple of convenient aliases for using the `history` command _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#history)_
-   **httpie** : Add completion for `httpie` _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#httpie)_
-   **pip** : Completion for the `python-pip` command _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#pip)_
-   **sudo** : <kbd>Esc</kbd> twice puts `sudo` in front of currentt command _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#sudo)_
-   **urltools** : Adds command line utilities for URL manipulation _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#urltools)_
-   **wd** : "Warp Directory" lets you to jump to custom dirs directly _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#wd)_
-   **web-search** : Adds several commands to do web search _[(link)](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#web-search)_
-   **z** : Cd into frequently used dirs (see man, its important !) _[(link)](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/z)_
-   **zsh-autosuggestions** : Add _fish-like_ suggestions in termial _[(link)](https://github.com/zsh-users/zsh-autosuggestions)_
-   **zsh-completions** : Additional completion definitions for Zsh _[(link)](https://github.com/zsh-users/zsh-completions)_

### Plugins load with `source`

-   **auto-fu** : Zsh automatic complete-word and list-choices _[(link)](https://github.com/hchbaw/auto-fu.zsh)_
-   **autopair** : Auto close delimiters, brackets, quotes etc _[(link)](https://github.com/hlissner/zsh-autopair)_
-   **calc** : allows you to perform calculs with only `=` command _[(link)](https://github.com/arzzen/calc.plugin.zsh)_
-   **fzf** : Commad-line fuzzy finder _[(link)](https://github.com/junegunn/fzf)_
-   **zce** : Add vim's Easymotion / emacs' Ace-jump-mode in zsh _[(link)](https://github.com/hchbaw/zce.zsh)_
-   **zsh-nohup** : Add `nohup <command> &> <command>.out &!` arround the <command> by hiting <kbd>Ctrl</kbd> + <kbd>h</kbd> <em> <a href="https://github.com/micrenda/zsh-nohup">(link)</a></em></command>
-   **zsh-you-should-use** : Simple zsh plugin that reminds you that you should use one of your existing aliases for a command you just typed _[(link)](https://github.com/MichaelAquilina/zsh-you-should-use)_

## Exports

-   `export HIST_STAMPS="yyyy-mm-dd"` : Display the command timestamp in history
-   `export CDPATH=.:~:~/Courses` : Allows you to perform a `cd` from multiples places (for example, if I am in "Courses" dir, I can do `cd Music` with "Music" placed in my home)

## Aliases
There is a small list of aliases (the most usefulls)
-   **duhs** : `du -sh * | sort -rh` is to display sizes of elements in current dir sorted by sizes
-   **meteo** : `curl wttr.in/Clermont-Ferrand` to display weather in terminal (replace "clermont-ferrand" with your city)
-   **HL** : is replaced by `--help`
-   **G** : `| grep`
-   **NUL** : `> /dev/null 2>&1`
-   **start** : `sudo systemctl start`
-   **restart** : `sudo systemctl restart`
-   **status** : `sudo systemctl status`
-   **stop** : `sudo systemctl stop`

> **IMPORTANT** : some aliases overrides system commands like `ls` or `cat`... If any problems, please look in the `.zshrc` file ;)

## Functions
> Some of the functions are in my "$HOME/Scripts" folder. You can clone it here : ![https://framagit.org/phineas0fog/scripts](https://framagit.org/phineas0fog/scripts)

-   **bcp** : `cp` command but add `-r` option if required
-   **brm** : same as above but with `rm` command and add `sudo` if needed (with confirmation)
-   **transfer <file>** : upload a file to "transfer.sh" website and print link in terminal. Put the link in `.transferShLog` in your home too (if you clear the term ;) )
-   **see <file>** : open the <file> with the correct reader (for example, see <file>.png will open the file in viewnior)
-   **command_not_found_handler** : I don't want to spoil what this command do... ;)

There is some other commands but not very interesting...

## Sources
-   ![oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh/)
-   ![powerlevel9k](https://github.com/bhilburn/powerlevel9k/)
-   Some functions are not mine but sourced in files
-   Some ideas from reddit and dotfiles



